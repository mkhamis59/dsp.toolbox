﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using DSPAlgorithms.DataStructures;
using DSPAlgorithms.Algorithms;

namespace DSPComponentsUnitTest
{
    [TestClass]
    public class AdderTestCases
    {
        //Add 2 signals -- 1,2
        [TestMethod]
        public void TestMethod1()
        {
            // test case 1 ..
            var sig1 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal1.ds");
            var sig2 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal2.ds");


           var expectedOutput = UnitTestUtitlities.LoadSignal("TestingSignals/Adder_TestCase1.ds");

            Adder a = new Adder();
            a.InputSignals = new List<Signal>();
            a.InputSignals.Add(sig1);
            a.InputSignals.Add(sig2);

            a.Run();
         
            Assert.IsTrue(UnitTestUtitlities.SignalsSamplesAreEqual(a.OutputSignal.Samples, expectedOutput.Samples));
        }

        //Add 3 signals -- 1,2,3
        [TestMethod]
        public void TestMethod2()
        {
           
            var sig1 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal1.ds");
            var sig2 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal2.ds");
            var sig3 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal3.ds");



            var expectedOutput = UnitTestUtitlities.LoadSignal("TestingSignals/Adder_TestCase2.ds"); 

            Adder a = new Adder();
            a.InputSignals.Add(sig1);
            a.InputSignals.Add(sig2);
            a.InputSignals.Add(sig3);

            a.Run();

            Assert.IsTrue(UnitTestUtitlities.SignalsSamplesAreEqual(a.OutputSignal.Samples, expectedOutput.Samples));
        }

        //Add 5 signals
        [TestMethod]
        public void TestMethod3()
        {
            // test case 1 ..
            var sig1 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal1.ds");
            var sig2 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal2.ds");
            var sig3 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal3.ds");
            var sig4 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal4.ds");
            var sig5 = UnitTestUtitlities.LoadSignal("TestingSignals/Signal5.ds");


            var expectedOutput = UnitTestUtitlities.LoadSignal("TestingSignals/Adder_TestCase3.ds"); ; ;

            Adder a = new Adder();
            a.InputSignals.Add(sig1);
            a.InputSignals.Add(sig2);
            a.InputSignals.Add(sig3);

            a.Run();

            Assert.IsTrue(UnitTestUtitlities.SignalsSamplesAreEqual(a.OutputSignal.Samples, expectedOutput.Samples));
        }
    }
}
